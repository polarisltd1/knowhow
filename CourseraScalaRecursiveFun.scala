```scala
package recfun
 
object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
 
 
    println(balance("(())".toList))
    println(balance("(()".toList))
 
    println(balance("(if (zero? x) max (/ 1 x))".toList))
    println(balance("I told him (that it’s not (yet) done). (But he wasn’t listening)".toList))
    println(balance("The function should return false for the following strings:".toList))
 
    println(balance("  :-)".toList))
    println(balance("())(".toList))
 
 
 
  }
 
  /**
   * Exercise 1
  
Exercise 1: Pascal’s Triangle
The following pattern of numbers is called Pascal’s triangle.
 
 
    1
 
   1 1
 
  1 2 1
 
 1 3 3 1
 
1 4 6 4 1
 
   ...
 
The numbers at the edge of the triangle are all 1, and each number inside the triangle is the sum of the two numbers above it. Write a function that computes the elements of Pascal’s triangle by means of a recursive process.
Do this exercise by implementing the pascal function in Main.scala, which takes a column c and a row r, counting from 0 and returns the number at that spot in the triangle. For example, pascal(0,2)=1,pascal(1,2)=2 and pascal(1,3)=3.
 
def pascal(c: Int, r: Int): Int
 
 
   */
    def pascal(c: Int, r: Int): Int = if (c==0 || c==r) 1 else pascal(c-1,r-1)+pascal(c,r-1)
   
  /**
    *
    * Exercise 2: Parentheses Balancing
    *
    *  Write a recursive function which verifies the balancing of parentheses in a string, which we represent as a List[Char] not a String.
    *
 */
    def balance(chars: List[Char]): Boolean = {
        def isBalanced(inp: List[Char], stack: List[Char]): Boolean =
       {
         //println("="+inp+":"+stack+" ")
         if (inp.isEmpty) stack.isEmpty
         else if (inp.head == '(') isBalanced(inp.tail, inp.head :: stack)
         else if (inp.head == ')') !stack.isEmpty && stack.head == '(' && isBalanced(inp.tail, stack.tail)
         else
           isBalanced(inp.tail, stack)
       }
    isBalanced(chars,List())
  }
 
  /**
    *
    *  Exercise 3: Counting Change
    *
    *  Write a recursive function that counts how many different ways you can make change for an amount, given a list of coin denominations.
    *  For example, there are 3 ways to give change for 4 if you have coins with denomination 1 and 2: 1+1+1+1, 1+1+2, 2+2.
    *
    *  Do this exercise by implementing the countChange function inMain.scala. This function takes an amount to change, and
    *  a list of unique denominations for the coins.
    *
   */
    def countChange(money: Int, coins: List[Int]): Int =
      if (money == 0) 1
      else if (coins.isEmpty || money < 0) 0
      else countChange(money - coins.head, coins) + countChange(money, coins.tail)
  }
  ```
  