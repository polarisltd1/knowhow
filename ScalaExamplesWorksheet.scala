// Example main class
object Main extends App {
  println(Lists.max(List(1,3,2)))
}
//
// how to create map, add element into and then print one.
//
var capital = Map("US" -> "Washington", "France" -> "Paris") // map example
capital += ("Japan" -> "Tokyo")
capital("France")
//
// recursive function example
//
def factorial(x: BigInt): BigInt =
  if (x == 0) 1 else x * factorial(x - 1)
factorial(30)
//
//The predicate _.isUpper is an example of a function literal in Scala.10
//It describes a function that takes a character argument (represented by the
//  underscore character), and tests whether it is an upper case letter.11
"Abcdef".exists(_.isUpper) // did any character in string is uppercase?
//
// Example of function
//
def max(x: Int, y: Int): Int = {
  if (x > y) x
  else y
}
max(5,10)  // call it
//
//
//
def greet() = println("Hello, world!")
greet()  // call it!
//
// creating list + forEach loop
//
List("aaa","bbb").foreach(arg => println("="+arg))
List("ppppp","qqqqq").foreach((arg: String) => println(arg))
//
//special shorthand in Scala. If a function literal
//  consists of one statement that takes a single argument, you need not explicitly
//  name and specify the argument.1
List("sssss","tttt").foreach(println)  // almost like lambdas :)
//
//
//
var args = List("ggggg","hhhh")
for (arg <- args)  // arrow at opposite direction ..
  println(arg)
//The to in this example is actually a method that takes
//  one Int argument. The code 0 to 2 is transformed into the
// method call (0).to(2).1
 
//
// +, , *, and / can be used in method names
// written 1 + 2 using traditional
// method invocation syntax, (1).+(2).
//
 
1 + 2
 
for (i <- 0 to 2)
  println(i+" apple")
 
for (i <- 5.until(10) )
  println(i+" apple")
 
for (i <- 5 until 10 )
  println(i+" apple")
 
 
 
 
 
//
//
//
// ::: list concatenation .
List(1,2) ::: List(3,4)
// :: Creates a new List[String] with the
//    three values "Will", "fill", and "until"
1 :: List(2,3)
val list000001 = "Will" :: "fill" :: "until" :: Nil // makes list from strings
List("Will") ::: List("fill") ::: List("until") ::: Nil // concatenates lists
list000001(1) // second element
//
//
list000001.count(s => s.length == 4) // count elements with size 4
list000001.exists(s => s == "until")
list000001.filter(s => s.length == 4)
list000001.forall(s => s.endsWith("l"))
list000001.head
list000001.isEmpty
list000001.length
list000001.map(s => s + "y")
list000001.mkString(", ")
list000001.reverse
list000001.tail
List(5,4,3,2,1).max
List(5,4,3,2,1).min
List(5,4,3,2,1).sum
 
 
//list000002.sort((s, t) => s < t)
//
// TT UU PP LL EE SS
//
// Tuples. Like lists, tuples are immutable,
// but unlike lists, tuples can contain different types of elements.
val pair = (99, "Luftballons",33,"Google")
pair._1
// These _N numbers are one-based, instead of
//zero-based, because starting with 1 is a tradition set by other languages
//
// Sets
//
var jetSet = Set("Boeing", "Airbus")
jetSet += "Lear"
jetSet
jetSet.contains("Cessna")
//
// assert
//
val res = Array("zero", "one", "two").mkString("\n")
assert(res == "zero\none\ntwo")
//
//
val numbers = Array(1,2,3)
numbers.mkString("[", ",", "]")
//
println("*** reading file\n")
//
import scala.io.Source
Source.fromFile("/etc/hosts").getLines().mkString("\n")
 
val lines = Source.fromFile("/etc/hosts").getLines()
 
println("*** Locate longest line in list\n")
val longestLine = lines.reduceLeft(
  (a, b) => if (a.length > b.length) a else b
)
 
//
val spaces = " " * 5 // make 5 spaces!!!!
println("|"+spaces+"|")
//
//
object RunRational extends App
{
  println("Hello World")
}
//
//
//