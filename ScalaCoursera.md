# Scala coursera
```
this is code example
```
##adding scala test library for Coursera.

edit build.sbt
 
``` 
name := "scalatest"
version := "1.0"
libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.1.0" % "test"
```
 
Installing sbt

```
brew install sbt
``` 
Using SBT

```
cd <project dir with build.sbt>
console  // enter REPL, complete with ctl-d
compile
test
run
>> submit Coursera Scala for evaluation
submit polarisltd@gmail.com  W3BFs6yCutn1TMFv 
>> result in    https://www.coursera.org/learn/progfun1/programming/xIz9O/
```

Scala FX

<https://code.google.com/archive/p/scalafx/wikis/GettingStarted.wiki>

<http://www.scalafx.org/docs/quickstart/>

<https://github.com/scalafx/scalafx>
    
##Call By Name

Typically, parameters to functions are by-value parameters; that is, the value of the parameter is determined before it is passed to the function. But what if we need to write a function that accepts as a parameter an expression that we don't want evaluated until it's called within our function? For this circumstance, Scala offers call-by-name parameters.
A call-by-name mechanism passes a code block to the call and each time the call accesses the parameter, the code block is executed and the value is calculated.

##Tail Recursion

Tracing back bytecodes shows that Scala compiler understood that it was the tail recursion and transformed a recursive call of function into the loop, which, as we could see earlier, is not that easy to write by yourself.
 if recursive function has no tail recursion and then it can can cause a stack overflow.
In order for a recursive call to be tail recursive, the call back to the function must be the last action performed in the function.


## Testing


```
import org.scalatest.FunSuite
class ExampleSuite extends FunSuite {

  test("addition") {
    val sum = 1 + 1
    assert(sum === 2)
  }

  test("subtraction") {
    val diff = 4 - 1
    assert(diff === 3)
  }
}

```

<http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite>


