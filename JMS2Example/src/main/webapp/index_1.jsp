<%-- 
/*
 comment line
 */
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JMS and JNDI demo app</title>
    </head>
    <body>
        <h1>JMS 2.0 Send/Receive Message</h1>
        <p>use following Post call to send/receive message:
            
        POST    http://localhost:8080/JMS2Example/app/jms;msg=messageText
                        
        </p>
        <!--a href="TestServlet"/>Send and Receive the message.</a-->
        <h1>Lookup datasource with JNDI</h1>
        <a href="DataExampleJndi"/>Show database info.</a>
    </body>
</html>
