/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jms2example.jsfbean;

import com.example.jms2example.MessageSender;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author robertspolis
 */
@ManagedBean(name="indexBean")
// Managed bean name already defaults to the classname with 1st character lowercased.
@SessionScoped
public class IndexBean implements Serializable{

    @EJB MessageSender sender; 
    
    
    
    /**
     * Creates a new instance of IndexBean
     */
    public IndexBean() {
    }
    
    public void runServlet(String url){
    FacesContext context = FacesContext.getCurrentInstance();
    //String url = context.getExternalContext().getContextName()+servletName;  
    System.out.println("Entering runServlet() "+url);

    try {
       context.getExternalContext().dispatch(url);
    }catch (Exception e) {
       e.printStackTrace();
    }
    finally{
       context.responseComplete();
    }
  }
  
    
  public void sendJmsMessage(){
      String payload = "our message";
      System.out.println("Entering sendJmsMessage()");
       sender.sendMessage(payload);
      
  }  
    
    
  public void postViaJmsClient() throws Exception{
      
      String url = getAbsoluteApplicationUrl()+"/app";
              //FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
      // .getApplicationContextPath() = /JMS2Example/app
      String s = "this is message we sending.";
      System.out.println("Entering postViaJmsClient() "+url);
      Client client = ClientBuilder.newClient();
      WebTarget target = client.target(url);
      //Response postResp = testCreateId.request().
      Response result = target
              .path("jms") // additional path element
              .request(MediaType.TEXT_PLAIN)
              .post(Entity.entity(s, MediaType.TEXT_PLAIN));
      System.out.println("Restful result: "+result.getStatus());
  }  

String getAbsoluteApplicationUrl() throws URISyntaxException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        URI uri = new URI(request.getRequestURL().toString());
        URI newUri = new URI(uri.getScheme(), null,
                uri.getHost(),
                uri.getPort(),
                request.getContextPath().toString(),null, null);
        return newUri.toString();
 }  
  
  
  
}
