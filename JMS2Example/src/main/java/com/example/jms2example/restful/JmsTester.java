/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jms2example.restful;

import com.example.jms2example.MessageSender;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;
import javax.naming.InitialContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


// what is url:
// http://localhost:8080/JMS2Example/app/test1;msg=xxx
/*
WADL was found at URL:
http://localhost:8080/JMS2Example/app/application.wadl

path "app" comes from @ApplicationPath("app")

response 

<application>
<doc jersey:generatedBy="Jersey: 2.21 2015-08-14 21:41:51" />
<doc jersey:hint="This is simplified WADL with user and core resources only. To get full WADL with extended resources use the query parameter detail. Link: http://localhost:8080/JMS2Example/app/application.wadl?detail=true" />
<grammars />
<resources base="http://localhost:8080/JMS2Example/app/">
<resource path="jms">
<method id="test" name="GET">
<response>
<representation mediaType="text/plain" />
</response>
</method>
</resource>
</resources>
 </application>



*/

@Path("jms")
public class JmsTester {

/**
 * 
 * This is JMS Producer Restful endpoint
 * 
 * Path: GET http://localhost:8080/JMS2Example/app/jms/
 * 
 * with PathParam 
 * @Path("{msg}") 
 * public String test(@PathParam("msg") String msg) {   
 *     GET http://localhost:8080/JMS2Example/app/jms/messageText
 * 
 * with MatrixParam public String userName(@MatrixParam("msg") String msg) {
 * http://localhost:8080/JMS2Example/app/jms;msg=messageText  // split key=value with ; (semicolumn)

*/
   
   
@EJB MessageSender sender;  

@GET
@Produces(MediaType.TEXT_PLAIN)
// @PathParam injects the value of URI parameter that defined in @Path
// expression, into the method.
//public String userName(@MatrixParam("msg") String msg) {
public String sendMsg(@MatrixParam("msg") String msg) {       
return "Hello "+msg;
}


 @POST
 @Consumes(MediaType.TEXT_PLAIN)
 public void send(String payload) throws Exception {
       System.out.println("About to send msg.payload: "+payload);
       sender.sendMessage(payload);
 }





}
