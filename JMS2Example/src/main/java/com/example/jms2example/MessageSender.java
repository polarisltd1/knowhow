/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.jms2example;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

/**
 *
 * @author robertspolis
 */

@Stateless
public class MessageSender {

    @Inject
    @JMSConnectionFactory("java:comp/DefaultJMSConnectionFactory")
    JMSContext context;
    
    @Resource(mappedName="java:global/jms/myQueue")
    Queue queue;

    public void sendMessage(String message) {
        System.out.println("Sending...");
        context.createProducer().send(queue, message);
    }
}
