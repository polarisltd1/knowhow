
package com.example;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import xjc.SBK.SBKRevenueData;
import xjc.Tex.TECNext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author robertsp
 */
public class ReadXML {
    
public static void main(String[] args){
    new ReadXML().readTec();
    new ReadXML().readSBK();
    
}


public void readTec(){
try {
JAXBContext jc =
//Reading XML is accomplished by using a JAXBContext, one or
//more JAXB-annotated classes, and an Unmarshaller.
JAXBContext.newInstance(TECNext.class);
Unmarshaller u = jc.createUnmarshaller();
InputStream in =
new FileInputStream("src/TecNextDataSet_NoXSD.xml");
TECNext p = (TECNext)u.unmarshal(in);


p.getAreas().forEach(a -> System.out.println("Tec: "+a.getCode()+" "+a.getLabel()));

} catch (JAXBException | IOException ex) {
    ex.printStackTrace();
}
}


public void readSBK(){
try {
JAXBContext jc =
//Reading XML is accomplished by using a JAXBContext, one or
//more JAXB-annotated classes, and an Unmarshaller.
JAXBContext.newInstance(SBKRevenueData.class);
Unmarshaller u = jc.createUnmarshaller();
InputStream in =
new FileInputStream("src/SBKRevenueData.xml");
SBKRevenueData p = (SBKRevenueData)u.unmarshal(in);


p.getSBKRevenue().forEach(a -> System.out.println("SBK: "+a.getCustomerid()+" "+a.getLast13WeeksRevenueGbp()));

} catch (JAXBException | IOException ex) {
    ex.printStackTrace();
}
}



}